local Blocklist = {}
Blocklist.__index = Blocklist
local http_request = require "http.request"

setmetatable(Blocklist, {
  __call = function (cls, ...)
    return cls.new(...)
  end
})

local function file_exists(name)
  local file = io.open(name, "r")
  if file then
    file:close()
  end
  return file ~= nil
end

function Blocklist.new(list_name, dir)
  local self = setmetatable({}, Blocklist)

  self.list_name = list_name
  local list = string.format("%s.list", list_name)
  if not dir then
    dir = '.'
  end
  self.dir = dir
  self.blacklist = string.format("%s/blacklist.rpz", dir)
  self.whitelist = string.format("%s/whitelist.rpz", dir)
  self.gz_filename = string.format("%s.gz", list)
  self.gz_path = string.format("%s/%s", dir, self.gz_filename)
  self.rpz_path = string.format("%s/%s.rpz", dir, list_name)
  self.etag_path = string.format("%s/.etag", dir)
  self.url = string.format("https://gitlab.com/jonocoetzee/private-dns/raw/master/blocklist/%s", list)

  if file_exists(self.etag_path) then
    local file = io.open(self.etag_path, "r")
    self.etag = file:read()
    file:close()
  end

  return self
end

local function list_iter(list)
  local i = 0
  local n = table.getn(list)
  return function()
    i = i + 1
    if i <= n then
      return list[i]
    end
  end
end

local function build_list(list, filename, wildcard)
  local tpm_file = string.format("%s.tmp", filename)
  local file = io.open(tpm_file, 'w+')

  for domain in list do
    local line = string.format("%s\tCNAME\t.\n", domain)
    file:write(line)

    if wildcard then
      file:write(string.format("*.%s", line))
    end
  end

  file:close()
  os.rename(tpm_file, filename)
end

local function createIfSetElseDelete(list, location, wildcard)
  if next(list) == nil then
    os.remove(location)
  else
    build_list(list_iter(list), location, wildcard)
  end
end

function Blocklist:create_files(whitelist, blacklist)
  createIfSetElseDelete(whitelist, self.whitelist, true)
  createIfSetElseDelete(blacklist, self.blacklist, true)

  if not file_exists(self.rpz_path) then
    build_list(list_iter({}), self.rpz_path)
  end
end

local function loadRpzIfExits(action, path, monitor)
  if file_exists(path) then
    policy.add(policy.rpz(action, path, monitor))
  end
end

function Blocklist:enable()
  loadRpzIfExits(policy.PASS, self.whitelist, false)
  loadRpzIfExits(policy.DENY, self.blacklist, false)
  policy.add(policy.rpz(policy.DENY, self.rpz_path))
end

function Blocklist:get_url()
  return self.url
end

function Blocklist:get()
  local tpm_filename = string.format("%s/.tmp", self.dir)
  local tpm_file = io.open(tpm_filename, 'w+')

  local request = http_request.new_from_uri(self.url)
  request.headers:upsert("accept-encoding", "gzip")
  local headers, stream = assert(request:go(20 * second))

  assert(stream:save_body_to_file(tpm_file, 1 * minute))
  tpm_file:close()
  os.rename(tpm_filename, self.gz_path)
  print("download complete")

  self.etag = headers:get("etag")
  local etag_file = io.open(self.etag_path, "w+")
  etag_file:write(self.etag)
  etag_file:close()
end

function Blocklist:fetch()
  worker.coroutine(function()
    if self.etag then
      local request = http_request.new_from_uri(self.url)
      request.headers:upsert(":method", "HEAD")
      local headers, _ = assert(request:go(20 * second))

      if headers:get("etag") ~= self.etag then
        print(string.format("%s blocklist modified, downloading...", self.list_name))
        self:get()
      end
    else
      print(string.format("downloading %s blocklist", self.list_name))
      self:get()
    end
  end)
end

function Blocklist:watch()
  local notify = require('cqueues.notify')

  local watcher = notify.opendir(self.dir)
  watcher:add(self.gz_filename, notify.CREATE)

  worker.coroutine(function()
    for _, name in watcher:changes() do
      if name == self.gz_filename then
        local handle = io.popen(string.format("gzip -c -d %s", self.gz_path))
        build_list(handle:lines(), self.rpz_path)
        handle:close()
      end
    end
  end)

  local interval = 12 * hour
  event.after(interval, function(_)
    event.recurrent(interval, function(_)
      self:fetch()
    end)
  end)
end

return Blocklist
