package.path = string.format("../?.lua;%s", package.path)

if os.getenv('VERBOSE') then
  verbose(true)
end

net.listen(net.eth0.addr[1], 5353)
-- net.listen(net.eth0.addr[1], 8853, {tls=true}) -- Uncomment to enable
net.ipv6 = false

-- Disable priming because it's unnecessary in a forward-only resolver
modules.unload('priming')

-- Load Useful modules
modules = {
  'policy', -- Block queries to local zones/bad sites
  'rebinding < iterate', -- Block rebinding attacks
  'stats', -- Track internal statistics
  'hints > iterate', -- Static hints (e.g. /etc/hosts)
  predict = { -- Prefetch expiring/frequent records
    window = 30, -- 30 minute sampling window
    period = 24 * (60 / 30) -- track over a day
  }
}

modules.load('http')
http.config({tls = false})
net.listen(net.eth0.addr[1], 8080, { kind = 'webmgmt' })

-- Add /etc/hosts
hints.add_hosts()

-- Smaller cache size
cache.size = 100 * MB

local ip_versions = {}
-- If either IP_V4 is set or neither are set
if os.getenv("IP_V4") or not os.getenv("IP_V6") then
  ip_versions.v4 = true
end

if os.getenv("IP_V6") then
  ip_versions.v6 = true
end

local provider_name = os.getenv("DNS_PROVIDER")
if not provider_name then
  provider_name="cloudflare_malware"
else
  provider_name=string.lower(provider_name)
end

local providers = require('Providers')(provider_name, ip_versions)
local provider_config = providers:get_config()
if not provider_config then
  print(string.format("Provider %s not supported, exiting.", provider_name))
  os.exit()
end

if os.getenv("MIN_TTL") then
  cache.min_ttl(os.getenv("MIN_TTL"))
end

print(string.format(''
  .. 'Usage\n'
  .. '=====\n'
  .. '$ docker run -p53:5353 -p53:5353/udp -p80:8080 -v private-dns:/srv/knot-resolver/data '
  .. '--init -t jonocoetzee/private-dns\n'
  .. '\n'
  .. 'Port mapping:\n'
  .. '53 -> DNS protocol over UDP and TCP\n'
  .. '80 -> web interface (stats portal/REST interface)\n'
  .. '\n'
  .. 'Version Information\n'
  .. '===================\n'
  .. 'DNS Resolver: %s\n'
  .. 'libknot: %s\n'
  .. '\n'
  .. 'Configuration\n'
  .. '=============\n'
  .. 'DNS Provider: %s\n'
  .. 'IP addresses: %s\n'
  .. 'DNS over TLS: Enabled\n'
  .. 'Minimum TTL: %s',
  os.getenv("KNOT_RESOLVER_VERSION"), os.getenv("LIBKNOT_VERSION"), providers:get_name(),
  table.concat(providers:get_ips(), ", "), cache.min_ttl()
))

local function get_list_from_env(list_name)
  local list = {}

  local env_value = os.getenv(list_name)
  if env_value then
    for domain in string.gmatch(env_value, "[^%s,]+") do
      table.insert(list, domain)
    end
  end

  return list
end

if not os.getenv("ADBLOCK_DISABLED") then
  local blocklist = require('Blocklist')('ad+malware')

  blocklist:create_files(get_list_from_env("WHITELIST"), get_list_from_env("BLACKLIST"))
  blocklist:enable()
  print('Blocklist: On')
  print(string.format('Blocklist URL: %s', blocklist:get_url()))

  blocklist:fetch()
  blocklist:watch()
else
  print('Blocklist: Off')
end

print()

policy.add(policy.all(policy.TLS_FORWARD(provider_config)))
