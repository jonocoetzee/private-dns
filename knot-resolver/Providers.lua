local Providers = {}
Providers.__index = Providers

setmetatable(Providers, {
  __call = function (cls, ...)
    return cls.new(...)
  end
})

local configs = {
  ["cloudflare"] = {
    ["name"] = "Cloudflare",
    ["ip4"] = {'1.1.1.1', '1.0.0.1'},
    ["ip6"] = {'2606:4700:4700::1111', '2606:4700:4700::1001'},
    ["hostname"] = 'cloudflare-dns.com',
  },
  ["cloudflare_malware"] = {
    ["name"] = "Cloudflare (malware)",
    ["ip4"] = {'1.1.1.2', '1.0.0.2'},
    ["ip6"] = {'2606:4700:4700::1112', '2606:4700:4700::1002'},
    ["hostname"] = 'cloudflare-dns.com',
  },
  ["cloudflare_family"] = {
    ["name"] = "Cloudflare (family)",
    ["ip4"] = {'1.1.1.3', '1.0.0.3'},
    ["ip6"] = {'2606:4700:4700::1113', '2606:4700:4700::1003'},
    ["hostname"] = 'cloudflare-dns.com',
  },
  ["quad9"] = {
    ["name"] = "Quad9",
    ["ip4"] = {'9.9.9.9', '149.112.112.112'},
    ["ip6"] = {'2620:fe::fe', '2620:fe::9'},
    ["hostname"] = 'dns.quad9.net',
  },
  ["google"] = {
    ["name"] = "Google",
    ["ip4"] = {'8.8.8.8', '8.8.4.4'},
    ["ip6"] = {'2001:4860:4860::8888', '2001:4860:4860::8844'},
    ["hostname"] = 'dns.google',
  },
  ["cleanbrowsing_family"] = {
    ["name"] = "CleanBrowsing (Family)",
    ["ip4"] = {'185.228.168.168', '185.228.169.168'},
    ["ip6"] = {'2a0d:2a00:1::', '2a0d:2a00:2::'},
    ["hostname"] = 'family-filter-dns.cleanbrowsing.org',
  },
  ["cleanbrowsing_adult"] = {
    ["name"] = "CleanBrowsing (Adult)",
    ["ip4"] = {'185.228.168.10', '185.228.169.11'},
    ["ip6"] = {'2a0d:2a00:1::1', '2a0d:2a00:2::1'},
    ["hostname"] = 'adult-filter-dns.cleanbrowsing.org',
  },
  ["cleanbrowsing_security"] = {
    ["name"] = "CleanBrowsing (Security)",
    ["ip4"] = {'185.228.168.9', '185.228.169.9'},
    ["ip6"] = {'2a0d:2a00:1::2', '2a0d:2a00:2::2'},
    ["hostname"] = 'security-filter-dns.cleanbrowsing.org',
  },
  ["quadrant"] = {
    ["name"] = "Quadrant",
    ["ip4"] = {'12.159.2.159'},
    ["ip6"] = {'2001:1890:140c::159'},
    ["hostname"] = 'dns-tls.qis.io',
  },
  ["libreops"] = {
    ["name"] = "LibreOps",
    ["ip4"] = {'116.203.115.192'},
    ["hostname"] = 'dot.libredns.gr',
  },
}

function Providers.new(name, ip_versions)
  local self = setmetatable({}, Providers)

  self.ips = {}
  if ip_versions["v4"] then
    for _, ip in ipairs(configs[name]["ip4"]) do
      table.insert(self.ips, ip)
    end
  end
  if ip_versions["v6"] then
    for _, ip in ipairs(configs[name]["ip6"]) do
      table.insert(self.ips, ip)
    end
  end

  self.config = {}
  for _, ip in ipairs(self.ips) do
    table.insert(self.config, {ip, hostname = configs[name]["hostname"]})
  end

  self.name = configs[name]["name"]

  return self
end

function Providers:get_ips()
  return self.ips
end

function Providers:get_config()
  return self.config
end

function Providers:get_name()
  return self.name
end

return Providers
