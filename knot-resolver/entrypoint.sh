#!/usr/bin/env ash
# shellcheck shell=dash

set -euo pipefail

# Don't start gc immediately in case this is first run and cache doesn't exist yet
(interval=60; sleep $interval; /usr/local/sbin/kres-cache-gc -c "$DATA_DIR" -d "$(( interval*1000 ))" ) &

exec "$@"
