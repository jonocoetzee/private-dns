#!/usr/bin/env bash

set -eo pipefail

namespace="jonocoetzee"
name="private-dns"
tag="latest"
declare -a platforms
declare -A endpoints
declare -A platform_endpoints
cache_to=

usage () {
  echo "./build.sh --platform <platforms> [--tag <tag>] [--remote <platform>,...:context]"
  exit
}

add_to_enpoints () {
  if [ "${endpoints[$1]}" ]; then
    endpoints[$1]="${endpoints[$1]},$2"
  else
    endpoints[$1]="$2"
  fi
  platform_endpoints["$2"]=$1
}

cleanup() {
  docker buildx rm $name
  if [ "$cache_to" ] && [ -d "$cache_to" ]; then
    rm -rf "$cache_to"
  fi
}

while [ $# -gt 0 ]; do
  case $1 in
    --platform)
      platforms+=("$2")
      shift; shift
      ;;
    --tag)
      tag=$2
      shift; shift
      ;;
    --remote)
      IFS=':' read -r -a platform_mapping <<< "$2"
      if [ ${#platform_mapping[@]} != 2 ]; then
        echo "Malformed remote argument"
        echo "Example: --remote arm/v7,arm64:rpi"
        exit
      fi

      IFS=',' read -r -a platform_list <<< "${platform_mapping[0]}"
      for platform in "${platform_list[@]}"; do
        add_to_enpoints "${platform_mapping[1]}" "linux/$platform"
      done
      shift; shift
      ;;
    *)
      echo "Unrecognised option: $1";
      usage
  esac
done

if [ ${#platforms[@]} == 0 ]; then
  platforms=("amd64" "arm/v7" "arm/v6")
fi

platforms=("${platforms[@]/#/linux/}")
for platform in "${platforms[@]}"; do
  if [ -z "${platform_endpoints[$platform]}" ]; then
    add_to_enpoints "default" "$platform"
  fi
done

docker login
builder_created=
for endpoint in "${!endpoints[@]}"; do
  if [ -z "$builder_created" ]; then
    docker buildx create --name $name --platform "${endpoints[$endpoint]}" --use "$endpoint"
    trap "cleanup" EXIT
    builder_created=1
  else
    docker buildx create --name $name --platform "${endpoints[$endpoint]}" --append "$endpoint"
  fi
done

cache_from=".buildx-cache/$tag"
cache_to="$cache_from-out"
cache_args=("--cache-to=type=local,dest=$cache_to,mode=max")
if [[ -f "$cache_from/index.json" ]]; then
  cache_args+=("--cache-from=type=local,src=$cache_from")
fi

docker buildx build --platform "$(IFS=,; echo "${platforms[*]}")" -t $namespace/"$name":"$tag" --push "${cache_args[@]}" .
rm -rf "$cache_from"
mv "$cache_to" "$cache_from"
