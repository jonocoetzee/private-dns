#!/usr/bin/env pypy3

import argparse
import concurrent.futures
import os
import sys

import tldextract
import validators


def main():
    args = parse_arguments()

    apex_domains, subdomains = get_cleaned_domains_by_type_from_file(args.input_file)

    if not args.clean_only:
        pruned_subdomains, shadowing_apex_domains, www_domains = prune_nested_subdomains(apex_domains, subdomains, args.workers)
        for apex_domain in shadowing_apex_domains.intersection(set(www_domains)):
            pruned_subdomains.remove(f'www.{apex_domain}')

        print('Finished pruning')
        count_removed = len(subdomains) - len(pruned_subdomains)
        print(f'  Number of entries removed: {count_removed} ({round(count_removed / (len(subdomains) + len(apex_domains)) * 100)}%)')
        print(f'  Number of entries remaining: {len(pruned_subdomains) + len(apex_domains)}')

    if not args.dont_whitelist:
        apex_domains.extend([f'*.{apex}' for apex in (apex_domains if args.clean_only else shadowing_apex_domains)])

    write_output(apex_domains + pruned_subdomains, args.output_file)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Prune nested domains from a list of domains')
    parser.add_argument('input_file', type=argparse.FileType('r'), help='File containing list of domains to prune')
    parser.add_argument('output_file', type=argparse.FileType('w'), nargs='?', default=sys.stdout,
                        help='File to output pruned domains (defaults to stdout)')
    parser.add_argument('--workers', metavar='N', type=int, choices=range(1, os.cpu_count() + 1), nargs='?',
                        default=os.cpu_count(), help='Number of workers (defaults to number of cores/threads)')
    parser.add_argument('--clean-only', action='store_true')
    parser.add_argument('--dont-whitelist', action='store_true', help="Don't wildcard apex domains")
    return parser.parse_args()


def get_cleaned_domains_by_type_from_file(file):
    def get_cleaned_domains():
        for domain in (line.rstrip('\n') for line in file if not line.startswith('#')):
            if not validators.domain(domain):
                idna_domain = domain.encode('idna').decode('utf-8')
                if domain == idna_domain or not validators.domain(idna_domain):
                    print(f'  Removing invalid entry (invalid format): {domain}')
                    continue
                else:
                    print(f'  Replacing U-label with A-label: {domain} -> {idna_domain}')
                    domain = idna_domain

            extract = tldextract.extract(domain)
            if extract.suffix:
                yield 'subdomain' if extract.subdomain else 'apex', domain
            else:
                print(f'  Removing invalid entry (invalid suffix): {domain}')
        file.close()

    print('Reading input file and cleaning entries...')
    domains_by_type = {'apex': [], 'subdomain': []}
    for domain_type, valid_domain in get_cleaned_domains():
        domains_by_type[domain_type].append(valid_domain)

    print('Finished loading entries')
    print(f'  Number of apex domains found: {len(domains_by_type["apex"])}')
    print(f'  Number of subdomains found: {len(domains_by_type["subdomain"])}')
    return domains_by_type['apex'], domains_by_type['subdomain']


def prune_nested_subdomains(apex_domains, subdomains, workers):
    with concurrent.futures.ProcessPoolExecutor(max_workers=workers) as executor:
        futures = [executor.submit(removed_nested, apex_domains, chunk) for chunk in
                   (subdomains[i::workers] for i in range(workers))]
        print('Pruning subdomains of apex domains...')

        processed = []
        shadowing_apex_domains = set()
        www_domains = list()

        for future in concurrent.futures.as_completed(futures):
            pruned_domains, chunk_shadowing_apex_domains, chunk_www_domains = future.result()
            processed.extend(pruned_domains)
            www_domains.extend(chunk_www_domains)
            shadowing_apex_domains.update(chunk_shadowing_apex_domains)

        return processed, shadowing_apex_domains, www_domains


def removed_nested(apex_domains, subdomains):
    chunk_shadowing_apex_domains = set()
    chunk_www_domains = list()

    def not_nested(subdomain):
        for apex in apex_domains:
            if subdomain.endswith(f'.{apex}'):
                if subdomain == f'www.{apex}':
                    chunk_www_domains.append(apex)
                    return True
                chunk_shadowing_apex_domains.add(apex)
                return False
        return True

    return list(filter(not_nested, subdomains)), chunk_shadowing_apex_domains, chunk_www_domains


def write_output(pruned, file):
    print('Writing entries to output file')
    for domain in sorted(pruned):
        file.write(f'{domain}\n')
    file.close()


def print_stderr(func):
    def wrapped_func(*args, **kwargs):
        return func(*args, **kwargs, file=sys.stderr)

    return wrapped_func


print = print_stderr(print)

main()
