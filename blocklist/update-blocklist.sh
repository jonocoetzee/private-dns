#!/usr/bin/env bash

set -eo pipefail

usage()
{
  echo "Usage: update-pihole-list.sh [[--disable-git] [--disable-git-remote] [--disable-docker-pull] [--disable-prune] [--delete-cache] | [-h]]"
}

git=1
git_remote=1
docker_pull=1
prune=1
delete_cache=

while [ "$1" != "" ]; do
  case $1 in
    --disable-git )
      git=
      git_remote=
      ;;
    --disable-git-remote )
      git_remote=
      ;;
    --disable-docker-pull )
      docker_pull=
      ;;
    --disable-prune )
      prune=
      ;;
    --delete-cache )
      delete_cache=1
      ;;
    -h | --help )
      usage
      exit
      ;;
    * )
      usage
      exit 1
  esac
  shift
done

set -u

if [ "$git_remote" == "1" ]; then
  # `cd` to script directory so we can git git pull/push
  cd "$(dirname "$0")" || exit

  if ! git pull -q origin master; then
    echo "Unable to update local repo. Aborting."
    exit 1
  fi
fi

base_dir="$(cd "$(dirname "$0")" && pwd)"
data_dir="$base_dir/pihole-data"
gravity_list="$data_dir/gravity.list"
output_list="$base_dir/ad+malware.list"

if [ "$docker_pull" == "1" ]; then
  echo "Fetching latest image pihole image"
  docker pull pihole/pihole
  echo
fi

echo "Updating gravity.list"
docker run \
  -v "$data_dir/:/etc/pihole/" \
  --dns 1.1.1.1 \
  --entrypoint /opt/pihole/gravity.sh \
  --rm \
  pihole/pihole
if [ "$delete_cache" == "1" ]; then
  rm "$data_dir"/list.*
fi

gravity_list_status=$(git status -s "$gravity_list" | awk '{ print $1; }')

echo
if [ "$gravity_list_status" == "M" ]; then
  if [ "$prune" == "1" ]; then
    echo "Pihole adlist modified, pruning..."
    python3 "$base_dir/prune.py" "$gravity_list" "$output_list"
  else
    echo "Pihole adlist modified, cleaning up..."
    python3 "$base_dir/prune.py" --clean-only "$gravity_list" "$output_list"
  fi

  # If something went wrong during pruning, output list will be empty.
  # If this happens then don't add to git and rather abort
  if [ -s "$output_list" ]; then
    if [ "$git" == "1" ]; then
      git add "$gravity_list" "$output_list"
      git commit -m "Update adlist"

      if [ "$git_remote" == "1" ]; then
        git push origin master
      fi
    fi
  else
    echo "Output list is empty. Aborting."
  fi
else
  echo "No changes to adlist. Exiting."
fi
