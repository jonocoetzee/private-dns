# Private DNS

By default, DNS lookups are completed in the clear, allowing your ISP and anyone in between you and your DNS server to infer sites you visit and services you use. DNS over TLS is one of the three main mechanisms to combat this but it has the advantage that it's also an IETF standard (unlike DNSCrypt) and not a hack (like DNS over HTTPS). Unfortunately, it isn't as easy to set up as regular DNS and many clients don't support it yet. The intention of this container is to have a service you can drop on your local network that speaks DoT to upstream provider but exposes plain old DNS to the internal network. This implementation is designed for personal use (small caches, etc.) and supports Cloudflare, Quad9, Google and CleanBrowsing (Family, Adult and Security) DNS providers (defaults to Cloudflare).

**Note:** This assumes [all communication is over HTTPS](https://www.eff.org/https-everywhere "HTTPS Everywhere") and even then unless both browser and server support [ESNI](https://blog.cloudflare.com/esni/ "Encrypting SNI: Fixing One of the Core Internet Bugs") your ISP will still be able to infer sites you visit. This isn't a silver bullet for privacy.

## Features

-   Fast
-   Low memory
-   All queries forwarded via DNS over TLS
-   Block requests to malware and ad serving domains (same blocklist as pi-hole)
-   Block DNS rebinding attacks
-   Predicts common DNS queries to preemptively revalidate/fetch latest values and keep cache hot

## Platforms

This image is tested on both x86_64 (linux/amd64) and ARMv7 (linux/arm/v7). ARMv8 (AArch64) has known upstream problems with LuaJIT, once those are fixed then it should work.

## Running

1.  (optional) Configure host to use static IP address
2.  `$ docker run -p53:5353 -p53:5353/udp -p80:8080 -v private-dns:/srv/knot-resolver/data --init -t jonocoetzee/private-dns`
3.  (optional) configure DHCP to assign static IP address as DNS server

If you want to resolve an IP address statically, add to `/etc/hosts` by passing in the `--add-host` flag to `docker run`. DNS server will honour static entries set in `/etc/hosts`.

### Environment Variables

Setting these variables (regardless of value) will enable/disable the feature.

| Name               | Outcome                                                                                                                                                                                                                      |
| ------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `ADBLOCK_DISABLED` | Disable ad-blocking feature                                                                                                                                                                                                  |
| `DNS_PROVIDER`     | Possible values: `cloudflare`, `cloudflare_malware`, `cloudflare_family`, `quad9`, `google`, `cleanbrowsing_family`, `cleanbrowsing_adult`, `cleanbrowsing_security`, `quadrant`, `libreops` (default: `cloudflare_malware`) |
| `IP_V4`            | Enable IPv4 addresses for a provider, if neither `$IP_V4` nor `$IP_V6` are set then `$IP_V4` is enabled by default                                                                                                           |
| `IP_V6`            | Enable IPv6 addresses for provider                                                                                                                                                                                           |
| `WHITELIST`        | Comma or space delimited list of domains to whitelist                                                                                                                                                                        |
| `BLACKLIST`        | Comma or space delimited list of domains to blacklist                                                                                                                                                                        |
| `MIN_TTL`          | [Override minimum TTL for entries](https://00f.net/2019/11/03/stop-using-low-dns-ttls/ "Stop using ridiculously low DNS TTLs"). This technically violates DNS standards, use with care.                                      |
| `VERBOSE`          | Enable verbose Knot Resolver output                                                                                                                                                                                          |

## Implementation

-   Implemented using Knot Resolver (also used by Cloudflare to implement their DNS service)

## Modifications

-   Feel free to roll your own `config.lua` file if you want to change anything (like the service forwarded too), just map it into the container at `/opt/knot-resolver/config.lua`
