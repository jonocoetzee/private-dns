ARG KNOT_RESOLVER=v5.2.1
ARG LIBKNOT=v3.0.3
ARG LUAJIT=2.1-20201229
ARG LUAROCKS=3.5.0

FROM alpine:3.12 as base
RUN apk add build-base

FROM base as luajit

ARG LUAJIT
RUN wget -qO - "https://github.com/openresty/luajit2/archive/v${LUAJIT}.tar.gz" | tar x -z

WORKDIR "luajit2-${LUAJIT}"
RUN sed -i 's/$(CROSS)ar/$(CROSS)$(AR)/g' src/Makefile
RUN sed -i "s/#BUILDMODE= dynamic/BUILDMODE= dynamic/g" src/Makefile

ARG AR="gcc-ar"
ARG RANLIB="gcc-ranlib"
ARG LDFLAGS="-static-libgcc -flto -s"
ARG CFLAGS="-O3 -flto -g0"
RUN make -j"$(nproc)" --load-average="$(nproc)" PREFIX=/usr
RUN make install

FROM base as kdns
RUN apk add autoconf automake libtool lmdb-dev gnutls-dev

ARG LIBKNOT
RUN wget -qO - "https://gitlab.labs.nic.cz/knot/knot-dns/-/archive/${LIBKNOT}/knot-dns-${LIBKNOT}.tar.bz2" | tar x -j
WORKDIR "knot-dns-${LIBKNOT}"

ARG AR="gcc-ar"
ARG RANLIB="gcc-ranlib"
ARG LDFLAGS="-flto -s"
ARG CFLAGS="-O3 -flto -g0"
RUN autoreconf -if
RUN ./configure --disable-static --disable-fastparser --disable-documentation --disable-daemon --disable-utilities --disable-modules
RUN make -j"$(nproc)" --load-average="$(nproc)" install

FROM base as knot-resolver
RUN apk add meson ninja libuv-dev lmdb-dev gnutls-dev bash

ARG KNOT_RESOLVER
RUN wget -qO - "https://gitlab.labs.nic.cz/knot/knot-resolver/-/archive/${KNOT_RESOLVER}/knot-resolver-${KNOT_RESOLVER}.tar.bz2" | tar x -j

WORKDIR "/knot-resolver-${KNOT_RESOLVER}/modules/policy"
RUN wget -qO - https://github.com/cloudflare/lua-aho-corasick/archive/master.tar.gz | tar x -z
RUN rm -rf lua-aho-corasick \
    && mv lua-aho-corasick-master lua-aho-corasick

WORKDIR "/knot-resolver-${KNOT_RESOLVER}"
COPY --from=luajit /usr/local/lib /usr/local/lib
COPY --from=luajit /usr/local/include /usr/local/include
COPY --from=kdns /usr/local/lib /usr/local/lib
COPY --from=kdns /usr/local/include /usr/local/include

RUN meson build_dir -Dbuildtype=release -Ddebug=false -Dstrip=true -Db_lto=true -Duser=daemon -Dgroup=daemon -Dinstall_kresd_conf=disabled \
    -Ddefault_library=static -Dcpp_link_args="-static-libstdc++ -static-libgcc"
WORKDIR "/knot-resolver-${KNOT_RESOLVER}/build_dir"
RUN ninja
RUN ninja install

RUN mkdir -p /tmp/lib \
    && cp -d /usr/local/lib/libknot.so* /tmp/lib \
    && cp -d /usr/local/lib/libdnssec.so* /tmp/lib \
    && cp -d /usr/local/lib/libzscanner.so* /tmp/lib \
    && cp -R /usr/local/lib/knot-resolver /tmp/lib/ \
    && cp -d /usr/local/lib/libluajit-5.1.so* /tmp/lib

FROM base as lua-modules
RUN apk add lua5.1-dev curl unzip coreutils openssl-dev bsd-compat-headers m4

ARG LUAROCKS
RUN wget -qO - "https://luarocks.org/releases/luarocks-${LUAROCKS}.tar.gz" | tar x -z
WORKDIR "luarocks-${LUAROCKS}"
RUN ./configure
RUN make install

RUN mkdir /root/.luarocks \
    && luarocks config variables.CFLAGS --scope=user -- "$(luarocks config variables.CFLAGS) -O3 -flto -g0" \
    && luarocks config variables.LIBFLAG --scope=user -- "$(luarocks config variables.LIBFLAG) -flto -s"

ARG AR="gcc-ar"
ARG RANLIB="gcc-ranlib"
RUN luarocks install http
RUN rm -rf /usr/local/share/lua/5.1/luarocks

FROM alpine:3.12

LABEL maintainer="jon@thancoetzee.com"

ARG HOME_DIR="/srv/knot-resolver"
ENV DATA_DIR="${HOME_DIR}/data"
RUN apk add --no-cache gnutls libuv lmdb \
    && mkdir -p "${DATA_DIR}" \
    && chown -R daemon:daemon "${DATA_DIR}"

USER daemon:daemon

WORKDIR "${DATA_DIR}"
VOLUME ["${DATA_DIR}"]

EXPOSE 5353/UDP 5353/TCP 8080/TCP

ARG KNOT_RESOLVER
ARG LIBKNOT
ENV KNOT_RESOLVER_VERSION="${KNOT_RESOLVER}"
ENV LIBKNOT_VERSION="${LIBKNOT}"

LABEL knot_resolver="${KNOT_RESOLVER}"
LABEL libknot="${LIBKNOT_VERSION}"

COPY knot-resolver "${HOME_DIR}/"
COPY --from=lua-modules /usr/local/lib/lua /usr/local/lib/lua
COPY --from=lua-modules /usr/local/share/lua /usr/local/share/lua
COPY --from=knot-resolver --chown=daemon:daemon /usr/local/etc/knot-resolver /usr/local/etc/knot-resolver
COPY --from=knot-resolver /usr/local/sbin /usr/local/sbin
COPY --from=knot-resolver /tmp/lib /usr/local/lib

ENTRYPOINT ["/srv/knot-resolver/entrypoint.sh", "/usr/local/sbin/kresd"]
CMD ["-c", "/srv/knot-resolver/config.lua", "-q", "-n"]
